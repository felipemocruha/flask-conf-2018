import mongoengine as me
from flask import Flask

from core.db import connect_to_database
from core.routes import create_routes


def create_app(config):
    app = Flask(__name__)
    app.secret_key = config['SECRET_KEY']

    db_config = {
        'db': config['DB_NAME'],
        'host': config['DB_HOST'],
        'port': config['DB_PORT']
    }
    connect_to_database(me.connect, db_config)
    api = create_routes()
    app.register_blueprint(api)

    return app
