import os
import sys
import logging


def create_logger():
    logger = logging.Logger('comics', level=logging.INFO)
    handler = logging.StreamHandler(stream=sys.stdout)
    format = f'[%(asctime)s][%(levelname)s] | %(message)s]'
    handler.setFormatter(logging.Formatter(format))
    logger.addHandler(handler)
    return logger


def get_config():
    return {
        'SECRET_KEY': os.getenv('SECRET_KEY', 'CHANGE ME!!!'),
        'DB_HOST': os.getenv('DB_HOST', 'localhost'),
        'DB_PORT': int(os.getenv('DB_PORT', '27017')),
        'DB_NAME': os.getenv('DB_NAME', 'xkcd')
    }
