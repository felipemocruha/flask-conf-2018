from core import create_app
from core.utils import get_config


config = get_config()
app = create_app(config)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080, debug=True)
